import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    body{
        font-family: 'Lato', sans-serif;
    }
    .title{
        text-decoration: none;
        color: black;
        font-weight: 700;
        text-align: center;
    }
    .linkDetails{
        text-decoration: none;
        background: #01a3a4;
        color: white;
        position: absolute;
        bottom: 20px;
        padding: 0.5rem 1rem;
        border-radius: 20px;
        &:hover{
            background: white;
            color: #01a3a4;
            border: 1px solid #01a3a4;
        }
    }  
`

export default GlobalStyles