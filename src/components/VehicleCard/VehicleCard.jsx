import styled from 'styled-components';
import { Link } from 'react-router-dom';

const VehicleCard = (props) => {
  const {id, make_and_model, color, doors, kilometrage} = props

  return (
    <VehicleContainer>
      <div>
        <h3>{make_and_model} <span>#{id}</span></h3>  
        <p>Color: {color}</p>
        {doors < 2 && <p>{doors} door</p>}
        {doors >= 2 && <p>{doors} doors</p>}
        <p>{kilometrage} Km</p>
      </div>
      
      <Link className="linkDetails" key={id} to={`/${id}`}>See more details</Link>
      
    </VehicleContainer>
  )
}

//correction : déplacement du style à la fin car cela rend le code plus facile à lire
const VehicleContainer = styled.div`
  border-radius: 0.25rem;
  padding: 1rem 2rem;
  width: 250px;
  height: 220px;
  margin-bottom: 2rem;
  background: #efefef;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
  position: relative;
  h3{
    margin-bottom: 0.25rem;
  }
  p{
    margin-bottom: 0.25rem;
  }
`

export default VehicleCard
