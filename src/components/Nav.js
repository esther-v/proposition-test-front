import { Link } from "react-router-dom";
import styled  from "styled-components";

const Nav = () => {
    return (
        <StyledNav>
            <Link to="/" className="title">
                <h1>My Garage</h1>
            </Link>
        </StyledNav>
    )  
}

const StyledNav = styled.nav`
    background: #ecf0f1;
    display:flex;
    align-items: center;
    justify-content: center;
    height: 10vh;
`

export default Nav