import React from 'react';
import { useLocation } from 'react-router-dom';
import styled from "styled-components";

const VehicleDetails = (props) => {
  
  const cars = props.cars
  
  const location = useLocation()
  const car_id = Number(location.pathname.split('/')[1])
 
  const selectedCar = cars.find(car => car.id === car_id)
  
  return (
    <VehiclePageContainer>
      <DetailsContainer>
        <h2>{selectedCar.make_and_model} N°{selectedCar.id}</h2>
        <p>Type : {selectedCar.car_type}</p>
        <p>{selectedCar.doors} {selectedCar.doors ===1 ? "door" : "doors"}</p>
        <p>Color : {selectedCar.color}</p>
        
        <p className='specs'>Car specifications : {selectedCar.specs.map((spec, index )=> {return <li key={index}>{spec}</li>})}</p>

        <p>Transmission : {selectedCar.transmission}</p>
        <p className='km'>{selectedCar.kilometrage} Km</p>
        <p className='options'>This car has various options : {selectedCar.car_options.map((option, index )=> {return <span key={index}>{option} - </span>})}</p>
      </DetailsContainer>
    </VehiclePageContainer>
  )
}

const VehiclePageContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 90vh;
  background: url('https://images.unsplash.com/photo-1492144534655-ae79c964c9d7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1583&q=80');
`
const DetailsContainer = styled.div`

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 500px;
  padding: 2rem;
  box-shadow: rgba(17, 12, 46, 0.15) 0px 48px 100px 0px;
  background: white;
  border-radius: 0.25rem;
  h2{
    margin-bottom: 1.5rem;
  }
  .specs{
    margin: 1rem 0;
  }
  .options{
    font-style:italic;
    margin-top: 2rem;
  }
  .km{
    color: #01a3a4;
  }
`

export default VehicleDetails