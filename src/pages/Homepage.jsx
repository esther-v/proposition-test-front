import React, { useState } from "react";
import VehicleCard from "../components/VehicleCard/VehicleCard";
import styled from 'styled-components';

const Homepage = (props) => {
  //review : la variable d'état 'cars' et le useEffect utilisant getVehicles ont été passés dans App pour avoir les props accessibles dans tous les composants

  const cars = props.cars
 
  //review : j'ai changé le nom order pour filter car plus explicite et j'ai passé filter en variable d'état avec useState pour permettre de mettre à jour la valeur au click du bouton avec la fonction toggleFilterBlackWhite

  const [filter, setFilter] = useState(false)

  const toggleFilterBlackWhite = ()  =>{
    setFilter(!filter)
  }

  const [km, setKm] = useState("")

  const updateKm = e => {
    e.preventDefault()
    setKm(e.target.value)
  }

  //review : ce code de rendu commencant par if(order) se répètait alors je mets la condition ligne 47 au niveau du map qui vérifie si filter est à true et si la couleur de la voiture est blanche ou noire

  return (
    <StyledHomePage>
      <BtnFilter
        style={{ cursor: "pointer" }}
        onClick={toggleFilterBlackWhite}>
        {filter ? "Reset filter" : "Only black & white cars"} 
      </BtnFilter>

      <select name="km" id="km" onChange={updateKm}>
        <option value="all">Select a kilometric value</option>
        <option value="moreThan50K">More than 50K</option>
        <option value="between30and50K">Between 30K and 50K</option>
        <option value="under30K">Under 30K</option>             
      </select>

      <BackgroundDiv>
        <CardContainer>
          {cars.map((car) => {
            if (filter && car.color !== "Black" && car.color !== "White") return <> </>

            if(km==="under30K" && car.kilometrage >= 30000) return <> </>

            if(km==="between30and50K" && (car.kilometrage < 30000 || car.kilometrage > 50000)) return <> </>

            if(km==="moreThan50K" && car.kilometrage <= 50000) return <> </>;

            return <VehicleCard key={car.id} {...car} />
          })}
        </CardContainer>
      </BackgroundDiv>
    </StyledHomePage>
  )
}

//review : déplacement du style à la fin car cela rend le code plus facile à lire

const StyledHomePage = styled.div`
  padding: 1rem 2rem 0; 
`
const BackgroundDiv = styled.div`
  background-image: url('https://images.unsplash.com/photo-1526726538690-5cbf956ae2fd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80');
  background-repeat: no-repeat;
  background-size: cover;
  min-height: 100vh;
`

const CardContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding: 2rem;
`

const BtnFilter = styled.button`
  background: black;
  color: white;
  border: none;
  padding: 0.5rem 1rem;
  border-radius: 20px;
  margin-bottom: 1rem;
  margin-right: 3rem;
`

export default Homepage
