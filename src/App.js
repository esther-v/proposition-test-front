import {useState, useEffect} from "react";
import { getVehicles } from "./services/vehicles";
import Homepage from "./pages/Homepage";
import VehicleDetails from "./pages/VehicleDetails";
import Nav from "./components/Nav";
import GlobalStyles from "./components/GlobalStyles";
import {Route, Routes}  from 'react-router-dom';

function App() {
  const [cars, setCars] = useState(null)
  useEffect(() => {
    const fetchCars = async () => {
      setCars(await getVehicles())
    };
    fetchCars()
  }, [])

  if (!cars) {
    return <>...LOADING</>
  }

  return (
    <div className="App">
      <GlobalStyles/>
      <Nav/>
      <Routes>
        <Route path="/" element={<Homepage cars={cars}/>}/>  
        <Route path="/:id" element={<VehicleDetails cars={cars}/>}/>      
      </Routes>
    </div>   
  )
}

export default App
